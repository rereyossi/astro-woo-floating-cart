jQuery(document).ready(function($) {
  /*=================================================;
  /* HELPER FUNCTION
  /*================================================= */
  $.fn.hasData = function(key) {
    return typeof $(this).data(key) != "undefined";
  };

  $.fn.getData = function(key) {
    var data = false;
    if ($(this).data(key) != "undefined") {
      var data = $(this).data(key);
    }
    return data;
  };

  /*=================================================;
  /* FLOATING CART
  /*================================================= */
  $(".js-awc-cart").floating_cart();

  
});
