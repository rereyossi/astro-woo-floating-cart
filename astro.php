<?php
/**
 * Plugin Name: Astro Woo Floating Cart
 * Description: Show mini cart on button with ajax
 * Plugin URI:  https://webforia.id/astro-woo-floating-cart
 * Version:     1.0.0
 * Author:      Webforia Studio
 * Author URI:  https://webforia.id
 * Text Domain: astro_domain
 */

if (!defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly
define('ASTRO_WOO_FLOATING_CART_DIR', dirname(plugin_basename(__FILE__))); // plugin folder name
define('ASTRO_WOO_FLOATING_CART_BASE', plugin_basename(__FILE__)); // base File
define('ASTRO_WOO_FLOATING_CART_URL', plugin_dir_url(__FILE__)); // include script or style
define('ASTRO_WOO_FLOATING_CART_ROOT', plugin_dir_path(__FILE__)); // include file
define('ASTRO_WOO_FLOATING_CART_VERSION', '1.0.0'); // plugin version

// Require the main plugin file
include plugin_dir_path(__FILE__) . 'function.php'; // function framework
include plugin_dir_path(__FILE__) . 'core/core-init.php'; // class framework
include plugin_dir_path(__FILE__) . 'plugin.php'; // final class
include plugin_dir_path(__FILE__) . 'core/vendor/plugin-update-checker/plugin-update-checker.php';

// update from gitlab
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://gitlab.com/rereyossi/astro-woo-floating-cart',__FILE__,'astro-woo-floating-cart'
);

//Optional: If you're using a private repository, specify the access token like this:
$myUpdateChecker->setAuthentication('FLrV3N7kDqM8jypXbpfD');

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('stable_release');
