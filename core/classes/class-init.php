<?php
/**
 * main classess include
 *
 * @author webforia studio
 * @since 1.0.0
 */

namespace Astro;

class Classes_Init
{
    public function __construct()
    {
        $this->include_part();
    }

    public function include_part()
    {
        /** Classes */
        include_once dirname(__FILE__) . '/class-html.php';
        include_once dirname(__FILE__) . '/class-helper.php';
        include_once dirname(__FILE__) . '/class-ajax.php';

    }

}

new Classes_Init;
