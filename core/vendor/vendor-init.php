<?php
namespace Astro;

class Vendor_Init
{

    public function __construct()
    {
        $this->include_part();

    }

    public function include_part()
    {
        include dirname(__FILE__) . '/plugin-update-checker/plugin-update-checker.php';
    }

    // end class
}

new Vendor_Init;
